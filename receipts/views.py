from django.shortcuts import render, redirect
from .models import Receipt
from django.contrib.auth.decorators import login_required
from .forms import CreateReceipt


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipts = form.save
            receipts.purchaser = request.user
            receipts.save()
            return redirect("home")
    else:
        form = CreateReceipt()

        context = {
            "form": form,
        }
        return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    if request.method == "POST":
        form = category_list(request.POST)
        if form.is_valid():
            expense = form.save
            expense = Receipt.objects.filter(purchaser=request.user)
            expense.save()
            return redirect("home")

    context = {
        "expense_category": expense,
    }
    return render(request, "receipts/list.html", context)


@login_required
def account_list(request):
    if request.method == "POST":
        form = account_list(request.POST)
        if form.is_vallid():
            account = form.save
            account = account.objects.filter(purchaser=request.user)
            account.save()
            return redirect("home")

    context = {
        "account": account,
    }
    return render(request, "receipts/list.html", context)
