from django.forms import ModelForm
from .models import Receipt, ExpenseCategory, Account


class CreateReceipt(ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )
